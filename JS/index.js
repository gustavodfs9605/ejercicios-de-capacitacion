/** Variables scope */

var a = 10; // El uso de var hace que la variable sea de alcance " global"
let b = 11; // El uso de let hace que la variable sea de alcance " por bloque"

// un bloque se puede considerar la seccion de { }



/** Funciones */

// Funciones 
function suma(a ,b) {
    return a + b;
}
console.log( suma(10, 15));

// Funciones Fecha
const cuadrado = x => x*x;
console.log( cuadrado(9));


/** String´s */

var str = "Gato, Perro, Gallo";

/**
 * slice: parte el string original y toma el string desde un indece inicio, hasta un 
 * indece final.
 *  string.slice(indiceInicio, indeceFinal)
 */ 
console.log( str.slice( 1,3)); 

/**
 * trim: remueve los espacios en blanco
 */
console.log( str.trim())


/** Arreglos */

// Los arreglos son estructuras donde sus elementos pueden ser de distintos tipos

const autos = ["Saab", "Volvo", "BMW"];

//Para recorrer un arreglo podemos hacer uso de un foreach

autos.forEach( elem=> { console.log( elem)});

// Agregar y quitar elementos de un arreglo

autos.push("Bocho"); // Agrega al final del arreglo

autos.pop(); // saca el ultimo elemento del arreglo

autos.shift(); // Saca el primer elemento del arreglo

autos.unshift( "Tsuru", "Ferrari"); // Agrega los elementos al inicio de la lista

console.table( autos);

//Otra manera que se puede hacer es por medio de keys ( SOLO ITERA CON LAS LLAVES)

Object.keys(autos)

/** Objetos */

const miObjeto = {nombre:'Tsuru', edad:29, nacionalidad: 'MX'};
/** Nota.- Se declara const para los objetos ya que no se busca modificar ninguna
 *  de sus propiedades.
 */

for( const prop in miObjeto) {
    console.log(prop);
}

// Para agregar o eliminar alguna propiedad a un objeto, solo es es necesario que el objeto
// este declarado con let

let estudiante = { nombre: 'Gustavo', edad: 25}

estudiante.apellido = 'Flores';

console.table(estudiante)

delete estudiante.apellido;

console.table(estudiante)


console.log( typeof( miObjeto)) // Devuelve de que tipo es la variable

console.log( Array.isArray(autos)); //Devuelve si la variable es un Arreglo

// Agregar elementos al docuemento HTML

agregarElemento = () => { 
    const nuevoElem = document.createElement("div");
    const nuevoText = document.createTextNode("Hola");
    nuevoElem.appendChild(nuevoText);

    const elementoActual = document.getElementById("div1");
    document.body.insertBefore(elementoActual, nuevoElem);
}

//************ EJERCICIO DE JUEGO  *******/

var puntuaje = 10;

const guardarPuntuaje = ()=>{
    window.localStorage.setItem("puntuaje", puntuaje);
}

const obtenerPuntuaje = ()=>{
    let p = 10;
    if( (p = window.localStorage.getItem("puntuaje")) != null ) {
        puntuaje = p;
    
    }
}

const cambiarPuntuaje = ()=> {
    const spanPuntuaje = document.getElementById("puntuaje");
    spanPuntuaje.textContent = puntuaje;
}


const load = ()=> {
    obtenerPuntuaje();
    cambiarPuntuaje();
    const fantasma = document.getElementById("fantamista");
    fantasma.addEventListener( "mouseover", (event)=>{
        event.target.textContent = ">w<";
    })
    fantasma.addEventListener( "mouseout", (event)=>{
        event.target.textContent = "OwO";
    })


}

const tocar = ()=> {
    puntuaje--;
    if(puntuaje == 0 ) {
        puntuaje = 10;
        verModal(true, "Ganaste!!");
    }
    guardarPuntuaje();
    cambiarPuntuaje();
}


//************ EJERCICIO DE EDAD  *******/

const validarEdad = ()=>{
    const edad = document.getElementById('edad').value;
    const hoy = new Date();
    const difEdad = hoy.getFullYear() - new Date(edad).getFullYear();
    (difEdad >= 18)? esMayorEdad() : esMenorEdad();
}

const verModal  = ( ver = false , mensaje = '')=>{
    let selectModal = document.getElementById("modal");
    let divContent  = document.getElementById("contenido-modal");
    //selectModal.style.display = (!ver) ? 'none' : 'block';
    divContent.textContent = mensaje;
    if(ver) {selectModal.classList.add('modal-overlay-on') } else { selectModal.classList.remove('modal-overlay-on')}
    return ver;
}

const regresar= ()=>{
    verModal( false);
}

const esMayorEdad = ()=> {
    
    console.log("Es mayor de edad");  
} 

const esMenorEdad = ()=>{
    verModal( true, "Es menor de edad")
    console.log("Es menor de edad");
}