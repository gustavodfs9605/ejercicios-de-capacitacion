import { LitElement, html, css } from "lit";

class CardProducto extends LitElement {

    constructor() {
        super();
    }

    static get properties() {
        return {
            titulo : String,
            precio : Number,
            descripcion : String,
            img : String
        }
    }

    static get styles() {
        return css`
            .producto-card {
                width: 16em; 
            }

            .producto-header {
                display: flex;
                flex-direction: column;
            }

            .producto-img {
                width: 65%;
                height: 14em; 
                margin: 0 auto;
            }

            .producto-des {
                overflow: hidden;
                white-space: nowrap;
            }

        `
    }

    render() {
        return html`
            <section class="producto-card"> 
                <header class="producto-header">
                    <img src="${this.img}" class="producto-img"/>
                    <h5>${this.title}</h5>
                </header>
                <article class="producto-des">  
                    <p>${this.descripcion}</p>
                </article>
            </section>
        `;
    }

}

export default CardProducto;