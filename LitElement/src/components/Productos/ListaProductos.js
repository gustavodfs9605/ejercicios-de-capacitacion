import {LitElement, html, css} from 'lit';


class ListaProductos extends LitElement {

    constructor() {
        super();
    }

    connectedCallback() {
        super.connectedCallback();
        this._obtenerProductos();
    }

    static get properties() {
        return {
            productos : Array
        }
    }

    _obtenerProductos() {
        let event = new CustomEvent('loader-on', {
            bubbles: true,
            detail: {
                name: "Manz"
            }
        });
        this.dispatchEvent(event);
        fetch('https://fakestoreapi.com/products')
            .then( resp=> resp.json() )
            .then( json=> this.productos = json)
            .catch( error=> console.log( error))
    }
    /**
     * 
            async obtenerProductos() {
                const resp = await fetch('https://fakestoreapi.com/products');
                this.productos = resp.json();
            }
     */

    static get styles() {
        return css`
            .lista-producto {
                display: flex;
                flex-flow: wrap;
                gap: 2em;
                justify-content: space-evenly;
            }
        `;
    }

    render() {
        return html`
            <article class="lista-producto">
                ${ this.productos?.map( elem=> html`
                    <card-producto .title=${ elem.title}
                                .img=${ elem.image}
                                .descripcion=${elem.description}>
                    </card-producto>
                `)}
                </article> 
        `
    }
}

export default ListaProductos;