import { LitElement, html, css } from "lit";

class LoaderComponent extends LitElement {
    constructor() {
        super();
        this.addEventListener('loader-on', resp =>{
            console.log( resp);
        });
    }

    static get properties() {
        return {
            activado : Boolean,
        }
    }

    
    
    _activarLoader( activar ) {
        console.log( activar);
        
    }

    connectedCallback() {

    }

    render() {
        return html`
            <div id="loader">
                Hola
            </div>
        `;
    }
}

export default LoaderComponent;