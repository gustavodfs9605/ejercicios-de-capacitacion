import ListaProductos   from "./src/components/Productos/ListaProductos";
import CardProducto     from './src/components/Productos/CardProducto';
import LoaderComponent  from './src/components/Otros/LoaderComponent';

window.customElements.define('loader-component', LoaderComponent);
window.customElements.define('card-producto', CardProducto);
window.customElements.define('lista-productos', ListaProductos);