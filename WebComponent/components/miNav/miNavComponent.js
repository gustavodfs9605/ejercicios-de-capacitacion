export default class miNavComponent extends HTMLElement {

    constructor() {
        super();
        this.shadow = this.attachShadow({mode: 'open'});
    }


    static get observedAttributes() {
        
    } 

    __crearMenu( element, stringItem ) {
        let item;
        if( typeof( stringItem) === "string") {
            item = document.createElement('li');
            item.textContent = stringItem;
        } else {
            item = document.createElement('ul');
            stringItem.forEach( elem=> this.__crearMenu( item, elem))
        }
        element.appendChild( item);
    }

    set menu(valor) {
        let menu = document.createElement('ul');
        this.__crearMenu( menu, valor);
        this.shadow.appendChild(menu);
    }

    attributeChangedCallback( nombreAtributo, nuevoValor, viejoValor) {
        
    }

    connectedCallback() {
        this.innerHTML = `
            <nav>
                Menu
            </nav>
        `;
    }
}